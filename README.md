# PrestaShop 1.6  smartblog module with custom date 

Adds a field for custom date in admin. 
Sort posts by date_cust !!!

## Installation

You need to create date_cust in ps_smart_blog_post (DB).
Replace files for smartblog in site/modules/smartblog.

You can replace full folder or only changed files:

* /modules/smartblog/classes/SmartBlogPost.php
* /modules/smartblog/classes/BlogPostCategory.php
* /modules/smartblog/controllers/front/details.php
* /modules/smartblog/controllers/admin/AdminBlogPostController.php

After that you can use {date_cust} in your .tpl 

e.g. 

```
<div class="postInfo">
  <div class="date-added">{$post.date_cust|date_format: "%m, %Y"}</div>
</div>

```